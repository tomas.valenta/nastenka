const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "base/templates/*/*.html",
  ],
  theme: {
    extend: {
      fontFamily: {
        "sans": ["Roboto Condensed", defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [],
}
