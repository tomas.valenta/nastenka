#!/usr/bin/make -f

PYTHON   = python
VENV     = .venv
PORT     = 8009
SETTINGS = nastenka.settings.dev

.PHONY: help venv install build run migrations migrate

help:
	@echo "Setup:"
	@echo "  venv           Setup virtual environment"
	@echo "  install        Install dependencies to venv"
	@echo "  build          Build CSS and JS files"
	@echo ""
	@echo "Application:"
	@echo "  run            Run the application on port ${PORT}"
	@echo ""
	@echo "Database:"
	@echo "  migrations     Generate migrations"
	@echo "  migrate        Run migrations"

venv: .venv/bin/python
.venv/bin/python:
	${PYTHON} -m venv ${VENV}

install: venv
	${VENV}/bin/pip install -r requirements/base.txt
	${VENV}/bin/nodeenv --python-virtualenv --node=19.3.0
	${VENV}/bin/npm install


build: venv
	${VENV}/bin/npm run build
	${VENV}/bin/python manage.py collectstatic --noinput --settings=${SETTINGS}

run: venv
	${VENV}/bin/python manage.py runserver ${PORT} --settings=${SETTINGS}

migrations: venv
	${VENV}/bin/python manage.py makemigrations --settings=${SETTINGS}

migrate: venv
	${VENV}/bin/python manage.py migrate --settings=${SETTINGS}
