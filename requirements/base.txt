django==4.1.4
django-database-url==1.0.3
python-dotenv==0.21.0
psycopg-binary==3.1.6
django-webpack-loader==1.8.0
nodeenv==1.7.0
