const path = require('path');
const BundleTracker = require('webpack-bundle-tracker');

module.exports = {
  mode: "production",
  context: __dirname,
  entry: path.resolve("static_src", "base.js"),
  output: {
    path: path.resolve(__dirname, "base", "static", "base"),
    filename: "[name]-[fullhash].js",
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  plugins: [
    new BundleTracker({filename: './webpack-stats.json'})
  ],
};
