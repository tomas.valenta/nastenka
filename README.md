# Nástěnka

A web dashboard for the Czech Pirate Party, merging all of its systems'
notifications and actions into a single highly customizeable dashboard.

## Basic requirements
- `make`
- `python`, 3.9 or newer
- `python-virtualenv`
- A PostgreSQL server

## Setup

Copy `env.example` to `.env`.

Set the ``DATABASE_URL`` environment variable in `.env` to one you can access your database server with. The format should be  as per RFC 1738, such as `postgresql://login:password@localhost:5432/database_name`. It's also important to change the ``SECRET_KEY`` variable.

Then, run the following commands:

```bash
make venv     # Create virtual environment
make install  # Install Python and Node.js dependencies
make build    # Build static files
```

## Running

To run with the default settings on the port set in `Makefile`, run:

```bash
make run
```

For more customization, it's better practice to run the server starting command directly:

```bash
source .venv/bin/activate   # Load the virtual environment
python manage.py runserver  # [ Your settings here ]
```
