import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';

window.addEventListener(
    "DOMContentLoaded",
    event => {
        // Add tooltips
        for (const element of document.querySelectorAll('.__tooltipped')) {
            tippy(
                element,
                {content: element.getAttribute("aria-label")}
            )
        }
    }
);
